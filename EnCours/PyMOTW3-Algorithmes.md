Algorithmes
==========

Python inclut de nombreux modules pour implémenter de manière élégante et concise des algorithmes en utilisant le style le plus approprié à la tâche. Il prend en charge les styles purement procédural, orienté objet et fonctionnel, ces trois styles sont fréquemment mélangés dans différentes parties d'un même programme.

`[functools]()` inclut des fonctions pour créer des fonctions de décorateurs, permettant la programmation orientée aspect et la réutilisation de code au-delà de ce qu'une approche orientée objet traditionnelle prend en charge. Elle fournit aussi un décorateur de classe pour implémenter toutes les API de comparaison enrichies utilisant un raccourci, et d'objets `partial` pour créer des références aux fonctions avec leurs arguments inclus.

Le module `[itertools]()` inclut des fonctions pour créer et travailler avec des itérateurs et des générateurs utilisant la programmation fonctionnelle. Le module `[operator]()` élimine le besoin de nombreuses fonctions lambda triviales lors de l'utilisation d'un style de programmation fonctionnel en fournissant des interfaces fonctionnelles aux opérations natives telles que l'arithmétique ou la recherche d'élément.

Et plus besoin de maîtriser quel style est utilisé dans un programme, `[contextlib]()` facilite la gestion des ressources, de manière plus fiable, et plus concise. Combiner les gestionnaires de contextes et l'instruction `with` réduit le nombre de blocs `try:finally` et les niveaux d'indentation nécessaires, tout en veillant à ce que les fichiers, les sockets, les transactions de base de données et les autres ressources soient fermés et publiés au bon moment.

- [functools — Outils pour la Manipulation des Fonctions]()
- [itertools — Fonctions d'Itérateur]()
- [operator — Interface Fonctionnelle pour les Opérateurs Natifs]()
- [contextlib — Utilitaires du Gestionnaire de Contexte]()


functools — Outils pour la Manipulation des Fonctions
---------


**But** : Fonctions qui opèrent sur d'autres fonctions.

Le module `functools` fournit des outils pour adapter et étendre des fonctions et d'autres objets appelables, sans les réécrire complètement.


### Les Décorateurs #

Le principal outil fourni par le module `functool` est la classe `partial`, qui peut être utilisé pour « encapsuler » un objet appelable avec des arguments par défaut. L'objet résultant est lui-même appelable et peut être traité comme s'il s'agissait de la fonction d'origine. Il prend l'ensemble des mêmes arguments que l'original, et peut être invoqué avec des arguments extra positionnés ou nommés, tout aussi bien. `partial` peut être utilisée au lieu d'une `lambda` qui fournit des arguments par défaut à une fonction, tout en laissant certains arguments non spécifiés.

#### Objets Partial

Cet exemple montre deux simples objets `partial` pour la fonction `myfunc()`. La sortie de `show_details()` inclut les attributs `func`, `args` et `keywords` de l'objet partiel.

```python
# functools_partial.py
import functools


def myfunc(a, b=2):
    "Docstring for myfunc()."
    print('  called myfunc with:', (a, b))


def show_details(name, f, is_partial=False):
    "Show details of a callable object."
    print('{}:'.format(name))
    print('  object:', f)
    if not is_partial:
        print('  __name__:', f.__name__)
    if is_partial:
        print('  func:', f.func)
        print('  args:', f.args)
        print('  keywords:', f.keywords)
    return


show_details('myfunc', myfunc)
myfunc('a', 3)
print()

# Set a different default value for 'b', but require
# the caller to provide 'a'.
p1 = functools.partial(myfunc, b=4)
show_details('partial with named default', p1, True)
p1('passing a')
p1('override b', b=5)
print()

# Set default values for both 'a' and 'b'.
p2 = functools.partial(myfunc, 'default a', b=99)
show_details('partial with defaults', p2, True)
p2()
p2(b='override b')
print()

print('Insufficient arguments:')
p1()
```

À la fin de cet exemple, le premier `partial` créé est invoqué sans passer de valeur à `a`, causant une exception.

```shell
$ python3 functools_partial.py

myfunc:
  object: <function myfunc at 0x1007a6a60>
  __name__: myfunc
  called myfunc with: ('a', 3)

partial with named default:
  object: functools.partial(<function myfunc at 0x1007a6a60>,
b=4)
  func: <function myfunc at 0x1007a6a60>
  args: ()
  keywords: {'b': 4}
  called myfunc with: ('passing a', 4)
  called myfunc with: ('override b', 5)

partial with defaults:
  object: functools.partial(<function myfunc at 0x1007a6a60>,
'default a', b=99)
  func: <function myfunc at 0x1007a6a60>
  args: ('default a',)
  keywords: {'b': 99}
  called myfunc with: ('default a', 99)
  called myfunc with: ('default a', 'override b')

Insufficient arguments:
Traceback (most recent call last):
  File "functools_partial.py", line 51, in <module>
    p1()
TypeError: myfunc() missing 1 required positional argument: 'a'
```

#### Acquisition des Propriétés d'une Fonction

L'objet `partial` n'a pas d'attributs `__name__` ou `__doc__` par défaut, et sans ces attributs, les fonctions décorées sont plus difficiles à déboguer. Utiliser `update_wrapper()`, copie ou ajoute les attributs de la fonction originale vers l'objet `partial`.

```python
# functools_update_wrapper.py
import functools


def myfunc(a, b=2):
    "Docstring for myfunc()."
    print('  called myfunc with:', (a, b))


def show_details(name, f):
    "Show details of a callable object."
    print('{}:'.format(name))
    print('  object:', f)
    print('  __name__:', end=' ')
    try:
        print(f.__name__)
    except AttributeError:
        print('(no __name__)')
    print('  __doc__', repr(f.__doc__))
    print()


show_details('myfunc', myfunc)

p1 = functools.partial(myfunc, b=4)
show_details('raw wrapper', p1)

print('Updating wrapper:')
print('  assign:', functools.WRAPPER_ASSIGNMENTS)
print('  update:', functools.WRAPPER_UPDATES)
print()

functools.update_wrapper(p1, myfunc)
show_details('updated wrapper', p1)
```

Les attributs ajoutés au wrapper sont définis dans `WRAPPER_ASSIGNMENTS`, tandis que `WRAPPER_UPDATES` liste les valeurs à modifier.

```shell
$ python3 functools_update_wrapper.py

myfunc:
  object: <function myfunc at 0x1018a6a60>
  __name__: myfunc
  __doc__ 'Docstring for myfunc().'

raw wrapper:
  object: functools.partial(<function myfunc at 0x1018a6a60>,
b=4)
  __name__: (no __name__)
  __doc__ 'partial(func, *args, **keywords) - new function with
partial application\n    of the given arguments and keywords.\n'

Updating wrapper:
  assign: ('__module__', '__name__', '__qualname__', '__doc__',
'__annotations__')
  update: ('__dict__',)

updated wrapper:
  object: functools.partial(<function myfunc at 0x1018a6a60>,
b=4)
  __name__: myfunc
  __doc__ 'Docstring for myfunc().'
```

#### D'autres Appelables

Les partiels fonctionnent avec tout objet appelable, et pas seulement avec les fonctions autonomes.

```python
# functools_callable.py
import functools


class MyClass:
    "Demonstration class for functools"

    def __call__(self, e, f=6):
        "Docstring for MyClass.__call__"
        print('  called object with:', (self, e, f))


def show_details(name, f):
    "Show details of a callable object."
    print('{}:'.format(name))
    print('  object:', f)
    print('  __name__:', end=' ')
    try:
        print(f.__name__)
    except AttributeError:
        print('(no __name__)')
    print('  __doc__', repr(f.__doc__))
    return


o = MyClass()

show_details('instance', o)
o('e goes here')
print()

p = functools.partial(o, e='default for e', f=8)
functools.update_wrapper(p, o)
show_details('instance wrapper', p)
p()
```

Cet exemple crée des partiels depuis une instance de classe avec la méthode `__call__()`.

```shell
$ python3 functools_callable.py

instance:
  object: <__main__.MyClass object at 0x1011b1cf8>
  __name__: (no __name__)
  __doc__ 'Demonstration class for functools'
  called object with: (<__main__.MyClass object at 0x1011b1cf8>,
'e goes here', 6)

instance wrapper:
  object: functools.partial(<__main__.MyClass object at
0x1011b1cf8>, f=8, e='default for e')
  __name__: (no __name__)
  __doc__ 'Demonstration class for functools'
  called object with: (<__main__.MyClass object at 0x1011b1cf8>,
'default for e', 8)
```

#### Méthodes et Fonctions

Tandis que `partial()` renvoie un appelable prêt à être utilisé directement, `partialmethod()` renvoie un appelable prêt à être utilisé en tant que méthode non liée à un objet. Dans l'exemple suivant, la même fonction autonome est ajoutée deux fois en tant qu'attribut de `MyClass`, une fois en utilisant `partialmethod()` en tant que `method1()` et ensuite utilisant `partial()` en tant que `method2()`.

```python
# functools_partialmethod.py
import functools


def standalone(self, a=1, b=2):
    "Standalone function"
    print('  called standalone with:', (self, a, b))
    if self is not None:
        print('  self.attr =', self.attr)


class MyClass:
    "Demonstration class for functools"

    def __init__(self):
        self.attr = 'instance attribute'

    method1 = functools.partialmethod(standalone)
    method2 = functools.partial(standalone)


o = MyClass()

print('standalone')
standalone(None)
print()

print('method1 as partialmethod')
o.method1()
print()

print('method2 as partial')
try:
    o.method2()
except TypeError as err:
    print('ERROR: {}'.format(err))
```

`method1()` peut être appelée depuis une instance de `MyClass`, puis l'instance est passé comme premier argument, tout comme pour les méthodes normalement définies. `method2()` n'est pas configurée en tant que méthode liée, ainsi l'argument `self` doit être explicitement passé, ou l'appel causera une exception `TypeError`.

```shell
$ python3 functools_partialmethod.py

standalone
  called standalone with: (None, 1, 2)

method1 as partialmethod
  called standalone with: (<__main__.MyClass object at
0x1007b1d30>, 1, 2)
  self.attr = instance attribute

method2 as partial
ERROR: standalone() missing 1 required positional argument:
'self'
```

#### Acquisition des Propriétés de Fonction pour Décorateurs

Mettre à jour les propriétés d'un appelable encapsulé est spécialement utile lors de l'utilisation d'un décorateur, puisque la fonction transformée se retrouve avec les propriétés de la fonction « nue » d'origine.

```python
# functools_wraps.py
import functools


def show_details(name, f):
    "Show details of a callable object."
    print('{}:'.format(name))
    print('  object:', f)
    print('  __name__:', end=' ')
    try:
        print(f.__name__)
    except AttributeError:
        print('(no __name__)')
    print('  __doc__', repr(f.__doc__))
    print()


def simple_decorator(f):
    @functools.wraps(f)
    def decorated(a='decorated defaults', b=1):
        print('  decorated:', (a, b))
        print('  ', end=' ')
        return f(a, b=b)
    return decorated


def myfunc(a, b=2):
    "myfunc() is not complicated"
    print('  myfunc:', (a, b))
    return


# The raw function
show_details('myfunc', myfunc)
myfunc('unwrapped, default b')
myfunc('unwrapped, passing b', 3)
print()

# Wrap explicitly
wrapped_myfunc = simple_decorator(myfunc)
show_details('wrapped_myfunc', wrapped_myfunc)
wrapped_myfunc()
wrapped_myfunc('args to wrapped', 4)
print()


# Wrap with decorator syntax
@simple_decorator
def decorated_myfunc(a, b):
    myfunc(a, b)
    return


show_details('decorated_myfunc', decorated_myfunc)
decorated_myfunc()
decorated_myfunc('args to decorated', 4)
```

`functools` fournit un décorateur `wraps()` qui applique `update_wrapper()` à la méthode décorée.

```shell
$ python3 functools_wraps.py

myfunc:
  object: <function myfunc at 0x101241b70>
  __name__: myfunc
  __doc__ 'myfunc() is not complicated'

  myfunc: ('unwrapped, default b', 2)
  myfunc: ('unwrapped, passing b', 3)

wrapped_myfunc:
  object: <function myfunc at 0x1012e62f0>
  __name__: myfunc
  __doc__ 'myfunc() is not complicated'

  decorated: ('decorated defaults', 1)
     myfunc: ('decorated defaults', 1)
  decorated: ('args to wrapped', 4)
     myfunc: ('args to wrapped', 4)

decorated_myfunc:
  object: <function decorated_myfunc at 0x1012e6400>
  __name__: decorated_myfunc
  __doc__ None

  decorated: ('decorated defaults', 1)
     myfunc: ('decorated defaults', 1)
  decorated: ('args to decorated', 4)
     myfunc: ('args to decorated', 4)
```


### Comparaison #

Sous Python 2, les classes pouvaient définir une méthode `__cmp__()` qui renvoie `-1`, `0`, ou `1` selon que l'objet est plus petit que, égal à, ou plus grand que l'élément comparé. Python 2.1 introduisit l'API des méthodes de *comparaisons riches* (`__lt__()`, `__le__()`, `__eq__()`, `__ne__()`, `__gt__()`, et `__ge__()`), qui effectuent une opération de comparaison simple et renvoient une valeur booléenne. Python 3 a déprécié `__cmp__()` en faveur de ces nouvelles méthodes et `functools` fournit des outils pour faciliter l'écriture de classes conformes aux exigences en matière de comparaison.

#### Comparaisons Riche

L'API de comparaison riche est conçue pour permettre aux classes d'implémenter chaque test de comparaisons complexes de la manière la plus efficiente. Toutefois, pour les classes où la comparaison est relativement simple, il est inutile de créer manuellement chacune des méthodes de comparaisons riches. Le décorateur de classe `total_ordering()` prend une classe qui fournit certaines de ces méthodes, et ajoute les restantes.

```python
# functools_total_ordering.py
import functools
import inspect
from pprint import pprint


@functools.total_ordering
class MyObject:

    def __init__(self, val):
        self.val = val

    def __eq__(self, other):
        print('  testing __eq__({}, {})'.format(
            self.val, other.val))
        return self.val == other.val

    def __gt__(self, other):
        print('  testing __gt__({}, {})'.format(
            self.val, other.val))
        return self.val > other.val


print('Methods:\n')
pprint(inspect.getmembers(MyObject, inspect.isfunction))

a = MyObject(1)
b = MyObject(2)

print('\nComparisons:')
for expr in ['a < b', 'a <= b', 'a == b', 'a >= b', 'a > b']:
    print('\n{:<6}:'.format(expr))
    result = eval(expr)
    print('  result of {}: {}'.format(expr, result))
```

La classe doit fournir l'implémentation de `__eq__()` et d'une autre méthode de comparaison riches. Le décorateur ajoute les implémentations du reste des méthodes qui fonctionnent en utilisant les comparaisons fournies. Si la comparaison ne peut pas être faite, la méthode renvoie `NotImplemented` ainsi la comparaison peut essayer d'utiliser les opérateurs de comparaisons inverses sur un autre objet, avant d'échouer entièrement.

```shell
$ python3 functools_total_ordering.py

Methods:

[('__eq__', <function MyObject.__eq__ at 0x10139a488>),
 ('__ge__', <function _ge_from_gt at 0x1012e2510>),
 ('__gt__', <function MyObject.__gt__ at 0x10139a510>),
 ('__init__', <function MyObject.__init__ at 0x10139a400>),
 ('__le__', <function _le_from_gt at 0x1012e2598>),
 ('__lt__', <function _lt_from_gt at 0x1012e2488>)]

Comparisons:

a < b :
  testing __gt__(1, 2)
  testing __eq__(1, 2)
  result of a < b: True

a <= b:
  testing __gt__(1, 2)
  result of a <= b: True

a == b:
  testing __eq__(1, 2)
  result of a == b: False

a >= b:
  testing __gt__(1, 2)
  testing __eq__(1, 2)
  result of a >= b: False

a > b :
  testing __gt__(1, 2)
  result of a > b: False
```

#### Ordre de Classement

Puisque l'ancien style de fonctions de comparaison a été déprécié dans Python 3, l'argument `cmp` de fonctions telle que `sort()` n'est plus lui aussi supporté. Les vieux programmes qui utilisent les fonctions de comparaisons peuvent utiliser `cpm_to_key()` pour les convertir vers une fonction qui retourne une *clé de classement*, qui est utilisée pour déterminer la position dans la séquence finale.

```python
# functools_cmp_to_key.py
import functools


class MyObject:

    def __init__(self, val):
        self.val = val

    def __str__(self):
        return 'MyObject({})'.format(self.val)


def compare_obj(a, b):
    """Old-style comparison function.
    """
    print('comparing {} and {}'.format(a, b))
    if a.val < b.val:
        return -1
    elif a.val > b.val:
        return 1
    return 0


# Make a key function using cmp_to_key()
get_key = functools.cmp_to_key(compare_obj)

def get_key_wrapper(o):
    "Wrapper function for get_key to allow for print statements."
    new_key = get_key(o)
    print('key_wrapper({}) -> {!r}'.format(o, new_key))
    return new_key


objs = [MyObject(x) for x in range(5, 0, -1)]

for o in sorted(objs, key=get_key_wrapper):
    print(o)
```

Normalement `cmp_to_key()` serait utilisé directement, mais dans cet exemple, une fonction wrapper supplémentaire est introduite pour imprimer plus d'informations car la fonction `key` a été appelée.

La sortie montre que `sorted()` démarre par l'appel à `get_key_wrapper()` pour chaque élément dans la séquence afin de produire une clé. Les clés retournées par `cmp_to_key()` sont des instances d'une classe définie dans `functools` qui implémente l'API de comparaison riche en utilisant l'ancienne fonction de comparaison passée à l'intérieur. Après que toutes les clés sont créées, la séquence est triée par comparaison des clés.

```shell
$ python3 functools_cmp_to_key.py

key_wrapper(MyObject(5)) -> <functools.KeyWrapper object at
0x1011c5530>
key_wrapper(MyObject(4)) -> <functools.KeyWrapper object at
0x1011c5510>
key_wrapper(MyObject(3)) -> <functools.KeyWrapper object at
0x1011c54f0>
key_wrapper(MyObject(2)) -> <functools.KeyWrapper object at
0x1011c5390>
key_wrapper(MyObject(1)) -> <functools.KeyWrapper object at
0x1011c5710>
comparing MyObject(4) and MyObject(5)
comparing MyObject(3) and MyObject(4)
comparing MyObject(2) and MyObject(3)
comparing MyObject(1) and MyObject(2)
MyObject(1)
MyObject(2)
MyObject(3)
MyObject(4)
MyObject(5)
```

### Mise en cache #

Le décorateur `lru_cache()` encapsule une fonction dans un cache le moins récemment utilisé. Les arguments de la fonction sont utilisés pour construire une clé de hachage, qui est ensuite mis en correspondance avec le résultat. Les appels suivants avec les mêmes arguments récupéreront la valeur depuis le cache au lieu d'appeler la fonction. Le décorateur ajoute aussi des méthodes à la fonction pour qu'elle examine l'état du cache (`cache_info()`) et vide le cache (`cache_clear()`).

```python
# functools_lru_cache.py
import functools


@functools.lru_cache()
def expensive(a, b):
    print('expensive({}, {})'.format(a, b))
    return a * b


MAX = 2

print('First set of calls:')
for i in range(MAX):
    for j in range(MAX):
        expensive(i, j)
print(expensive.cache_info())

print('\nSecond set of calls:')
for i in range(MAX + 1):
    for j in range(MAX + 1):
        expensive(i, j)
print(expensive.cache_info())

print('\nClearing cache:')
expensive.cache_clear()
print(expensive.cache_info())

print('\nThird set of calls:')
for i in range(MAX):
    for j in range(MAX):
        expensive(i, j)
print(expensive.cache_info())
```

L'exemple fait de nombreux appels à `expensive()` dans un ensemble de boucles imbriquées. La seconde fois où les appels sont faits avec les mêmes valeurs, le résultat apparaît depuis le cache. Quand le cache est nettoyé et que les boucles sont à nouveau exécutées, les valeurs doivent être recalculées.

```shell
$ python3 functools_lru_cache.py

First set of calls:
expensive(0, 0)
expensive(0, 1)
expensive(1, 0)
expensive(1, 1)
CacheInfo(hits=0, misses=4, maxsize=128, currsize=4)

Second set of calls:
expensive(0, 2)
expensive(1, 2)
expensive(2, 0)
expensive(2, 1)
expensive(2, 2)
CacheInfo(hits=4, misses=9, maxsize=128, currsize=9)

Clearing cache:
CacheInfo(hits=0, misses=0, maxsize=128, currsize=0)

Third set of calls:
expensive(0, 0)
expensive(0, 1)
expensive(1, 0)
expensive(1, 1)
CacheInfo(hits=0, misses=4, maxsize=128, currsize=4)
```

Pour empêcher le cache de grandir sans limite dans un processus de longue durée, il lui est donnée une taille maximale. Par défaut, elle est de 128 entrées, mais elle peut être changée pour chaque cache en utilisant l'argument `maxsize`.

```python
# functools_lru_cache_expire.py
import functools


@functools.lru_cache(maxsize=2)
def expensive(a, b):
    print('called expensive({}, {})'.format(a, b))
    return a * b


def make_call(a, b):
    print('({}, {})'.format(a, b), end=' ')
    pre_hits = expensive.cache_info().hits
    expensive(a, b)
    post_hits = expensive.cache_info().hits
    if post_hits > pre_hits:
        print('cache hit')


print('Establish the cache')
make_call(1, 2)
make_call(2, 3)

print('\nUse cached items')
make_call(1, 2)
make_call(2, 3)

print('\nCompute a new value, triggering cache expiration')
make_call(3, 4)

print('\nCache still contains one old item')
make_call(2, 3)

print('\nOldest item needs to be recomputed')
make_call(1, 2)
```

Dans cet exemple, la taille du cache est de 2 entrées. Lorsque le troisième ensemble d'arguments unique (`3, 4`) est utilisé, le plus ancien élément dans le cache est supprimé et remplacé par le nouveau résultat.

```shell
$ python3 functools_lru_cache_expire.py

Establish the cache
(1, 2) called expensive(1, 2)
(2, 3) called expensive(2, 3)

Use cached items
(1, 2) cache hit
(2, 3) cache hit

Compute a new value, triggering cache expiration
(3, 4) called expensive(3, 4)

Cache still contains one old item
(2, 3) cache hit

Oldest item needs to be recomputed
(1, 2) called expensive(1, 2)
```

Les clés pour le cache géré par `lru_cache()` doivent être hachables, ainsi tous les arguments de la fonction encapsulée avec la recherche du cache doivent être hachable.

```python
# functools_lru_cache_arguments.py
import functools


@functools.lru_cache(maxsize=2)
def expensive(a, b):
    print('called expensive({}, {})'.format(a, b))
    return a * b


def make_call(a, b):
    print('({}, {})'.format(a, b), end=' ')
    pre_hits = expensive.cache_info().hits
    expensive(a, b)
    post_hits = expensive.cache_info().hits
    if post_hits > pre_hits:
        print('cache hit')


make_call(1, 2)

try:
    make_call([1], 2)
except TypeError as err:
    print('ERROR: {}'.format(err))

try:
    make_call(1, {'2': 'two'})
except TypeError as err:
    print('ERROR: {}'.format(err))
```

Si un objet, qui ne peut être haché, est passé à la fonction, une exception `TypeError` est levée.

```shell
$ python3 functools_lru_cache_arguments.py

(1, 2) called expensive(1, 2)
([1], 2) ERROR: unhashable type: 'list'
(1, {'2': 'two'}) ERROR: unhashable type: 'dict'
```


### Réduire un Ensemble de Données #

La fonction `reduce()` prend un appelable et une séquence de données en entrées et produit une seule valeur en sortie basée sur l'invocation de l'appelable, des valeurs de la séquence et l'accumulation de la sortie résultante.

```python
# functools_reduce.py
import functools


def do_reduce(a, b):
    print('do_reduce({}, {})'.format(a, b))
    return a + b


data = range(1, 5)
print(data)
result = functools.reduce(do_reduce, data)
print('result: {}'.format(result))
```

Cet exemple ajoute les nombres dans la séquence d'entrée.

```shell
$ python3 functools_reduce.py

range(1, 5)
do_reduce(1, 2)
do_reduce(3, 3)
do_reduce(6, 4)
result: 10
```

L'argument `initializer` optionnel est placé au début de la séquence et traité avec les autres éléments. il peut être utilisé pour mettre à jour une valeur précédemment calculée avec de nouvelles entrées.

```python
# functools_reduce_initializer.py
import functools


def do_reduce(a, b):
    print('do_reduce({}, {})'.format(a, b))
    return a + b


data = range(1, 5)
print(data)
result = functools.reduce(do_reduce, data, 99)
print('result: {}'.format(result))
```

Dans cet exemple, une somme précédente de `99` est utilisée pour initialiser la valeur calculée par `reduce()`.

```shell
$ python3 functools_reduce_initializer.py

range(1, 5)
do_reduce(99, 1)
do_reduce(100, 2)
do_reduce(102, 3)
do_reduce(105, 4)
result: 109
```

Les séquences avec un seul élément se réduisent automatiquement à cette valeur lorsqu'aucun initialiseur n'est présent. Vider les listes génère une erreur, à moins qu'un initialiseur soit fourni.

```python
# functools_reduce_short_sequences.py
import functools


def do_reduce(a, b):
    print('do_reduce({}, {})'.format(a, b))
    return a + b


print('Single item in sequence:',
      functools.reduce(do_reduce, [1]))

print('Single item in sequence with initializer:',
      functools.reduce(do_reduce, [1], 99))

print('Empty sequence with initializer:',
      functools.reduce(do_reduce, [], 99))

try:
    print('Empty sequence:', functools.reduce(do_reduce, []))
except TypeError as err:
    print('ERROR: {}'.format(err))
```

Parce que l'argument `initializer` sert de valeur par défaut, mais qu'il est aussi combiné avec les nouvelles valeurs si la séquence d'entrée n'est pas vide, il est important de considérer avec soin s'il faut l'utiliser. Quand cela n'a pas de sens de combiner la valeur par défaut avec les nouvelles valeurs, il est préférable de capturer l'exception `TypeError` plutôt que passer un initialiseur.

```shell
$ python3 functools_reduce_short_sequences.py

Single item in sequence: 1
do_reduce(99, 1)
Single item in sequence with initializer: 100
Empty sequence with initializer: 99
ERROR: reduce() of empty sequence with no initial value
```


### Fonctions Génériques #

Dans un langage typé dynamiquement tel que Python, il est courant de devoir effectuer une opération légèrement différente en fonction du type d'un argument, en particulier lorsqu'il s'agit de la différence entre une liste d'éléments et un seul élément. Il est assez simple de vérifier le type d'un argument directement, mais dans les cas où la différence de comportement peut être isolée dans des fonctions distinctes, `functools` fournit au décorateur `singledispatch()` l’enregistrement d’un ensemble de *fonctions génériques* pour la commutation automatique en fonction du type du premier argument d’une fonction.

```python
# functools_singledispatch.py
import functools


@functools.singledispatch
def myfunc(arg):
    print('default myfunc({!r})'.format(arg))


@myfunc.register(int)
def myfunc_int(arg):
    print('myfunc_int({})'.format(arg))


@myfunc.register(list)
def myfunc_list(arg):
    print('myfunc_list()')
    for item in arg:
        print('  {}'.format(item))


myfunc('string argument')
myfunc(1)
myfunc(2.3)
myfunc(['a', 'b', 'c'])
```

L'attribut `register()` de la nouvelle fonction sert comme un autre décorateur pour enregistrer les implémentations alternatives. La première fonction encapsulée avec `singledispatch()` est l'implémentation par défaut si aucune autre fonction spécifique au type n'est trouvée, comme c'est le cas avec `float` dans cet exemple.

```shell
$ python3 functools_singledispatch.py

default myfunc('string argument')
myfunc_int(1)
default myfunc(2.3)
myfunc_list()
  a
  b
  c
```

Quand aucune correspondance exacte n'est trouvée pour le type, l'ordre inhérent est évalué et le type correspondant le plus proche est utilisé.

```python
# functools_singledispatch_mro.py
import functools


class A:
    pass


class B(A):
    pass


class C(A):
    pass


class D(B):
    pass


class E(C, D):
    pass


@functools.singledispatch
def myfunc(arg):
    print('default myfunc({})'.format(arg.__class__.__name__))


@myfunc.register(A)
def myfunc_A(arg):
    print('myfunc_A({})'.format(arg.__class__.__name__))


@myfunc.register(B)
def myfunc_B(arg):
    print('myfunc_B({})'.format(arg.__class__.__name__))


@myfunc.register(C)
def myfunc_C(arg):
    print('myfunc_C({})'.format(arg.__class__.__name__))


myfunc(A())
myfunc(B())
myfunc(C())
myfunc(D())
myfunc(E())
```

Dans cet exemple, les classes `D` et `E` n'ont pas de correspondances exactes avec aucune des fonctions génériques enregistrées, la fonction choisie dépend de la hiérarchie de classe.

```shell
$ python3 functools_singledispatch_mro.py

myfunc_A(A)
myfunc_B(B)
myfunc_C(C)
myfunc_B(D)
myfunc_C(E)
```

### Lire aussi #

- [Documentation de la bibliothèque standard pour functools](https://docs.python.org/fr/3.7/library/functools.html)
- [Méthode de comparaison riche](https://docs.python.org/fr/3/reference/datamodel.html#object.__lt__) — Description des méthodes de comparaisons riches depuis le Guide de Référence Python.
- [@memoize isolé](http://nedbatchelder.com/blog/201601/isolated_memoize.html "Isolated @memoize") — Article, en anglais, à-propos de la création de décorateurs memo qui fonctionnent bien avec les tests unitaires, par Ned Batchelder.
- **[PEP 443](https://www.python.org/dev/peps/pep-0443 "Single-dispatch generic functions")** — "Fonctions génériques à distribution unique"
- [inspect]() — API d'introspection pour les objets vivants.


itertools — Fonctions d'Itérateur
---------


**But** : Le module `itertools` inclut un ensemble de fonctions pour travailler avec des ensembles de données de séquences.

Les fonctions fournies par `itertools` sont inspirées de fonctionnalités similaire de langages de programmation fonctionnels tels que Clojure, Haskell, APL, et SML. Elles sont destinés à être rapides et à utiliser efficacement la mémoire, de même pour fonctionner ensemble et exprimer des algorithmes plus complexes basés sur l'itération.

Le code basé sur l'itération offre de meilleures caractéristiques de consommation de mémoire que du code qui utilise des listes. Puisque les données ne sont pas produites à partir de l'itérateur jusqu'à ce qu'elles soient nécessaires, toutes les données n'ont pas besoin d'être stockées en mémoire en même temps. Ce modèle de traitement « paresseux » peut réduire la permutation et d’autres effets secondaires d’importants ensembles de données, améliorant ainsi les performances.

En plus de ces fonctions définies dans `itertools`, les exemples dans ce chapitre s'appuient aussi sur certaines des fonctions natives pour l'itération.


### Fusion et Division d'Itérateurs #

La fonction `chain()` prend de nombreux itérateurs comme arguments et renvoie un itérateur unique qui produit le contenu de toutes les entrées comme si elles venaient d'un seul itérateur.

```python
# itertools_chain.py
from itertools import *

for i in chain([1, 2, 3], ['a', 'b', 'c']):
    print(i, end=' ')
print()
```

`chain()` facilite le processus de nombreuses séquences sans construire une grande liste.

```shell
$ python3 itertools_chain.py

1 2 3 a b c
```

Si les itérables qui doivent être combinés ne sont pas connus à l'avance, ou ont besoin d'être évalués paresseusement, `chain.from_iterable()` peut être utilisée pour construire la chaîne à la place.

```python
# itertools_chain_from_iterable.py
from itertools import *


def make_iterables_to_chain():
    yield [1, 2, 3]
    yield ['a', 'b', 'c']


for i in chain.from_iterable(make_iterables_to_chain()):
    print(i, end=' ')
print()
```

```shell
$ python3 itertools_chain_from_iterable.py

1 2 3 a b c
```

La fonction native `zip()` renvoie un itérateur qui combine les éléments de nombreux itérateurs dans des tuples.

```python
# itertools_zip.py
for i in zip([1, 2, 3], ['a', 'b', 'c']):
    print(i)
```

Tout comme avec les autres fonctions dans ce module, la valeur retournée est un objet itérable qui produit des valeurs une à la fois.

```shell
$ python3 itertools_zip.py

(1, 'a')
(2, 'b')
(3, 'c')
```

`zip()` s'arrête lorsque le premier itérateur d'entrée est épuisé. Pour traiter toutes les entrées, même si l'itérateur produit différents nombres de valeurs, utilisez `zip_longest()`.

```python
# itertools_zip_longest.py
from itertools import *

r1 = range(3)
r2 = range(2)

print('zip stops early:')
print(list(zip(r1, r2)))

r1 = range(3)
r2 = range(2)

print('\nzip_longest processes all of the values:')
print(list(zip_longest(r1, r2)))
```

Par défaut, `zip_longest()` substitue `None` à toutes les valeur manquantes. Utilisez l'argument `fillvalue` pour utiliser un valeur de substitut différente.

```shell
$ python3 itertools_zip_longest.py

zip stops early:
[(0, 0), (1, 1)]

zip_longest processes all of the values:
[(0, 0), (1, 1), (2, None)]
```

La fonction `islice()` renvoie un itérateur qui retourne les éléments sélectionnés

```python
# itertools_islice.py
from itertools import *

print('Stop at 5:')
for i in islice(range(100), 5):
    print(i, end=' ')
print('\n')

print('Start at 5, Stop at 10:')
for i in islice(range(100), 5, 10):
    print(i, end=' ')
print('\n')

print('By tens to 100:')
for i in islice(range(100), 0, 100, 10):
    print(i, end=' ')
print('\n')
```

`islice()` prend les mêmes arguments que l'opérateur slice pour les listes : `start`, `stop`, et `step`. Les arguments start et step sont optionnels.

```shell
$ python3 itertools_islice.py

Stop at 5:
0 1 2 3 4

Start at 5, Stop at 10:
5 6 7 8 9

By tens to 100:
0 10 20 30 40 50 60 70 80 90
```

La fonction `tee()` renvoie de nombreaux itérateurs indépendants (par défaut : 2) basés sur une unique entrée originale.

```python
# itertools_tee.py
from itertools import *

r = islice(count(), 5)
i1, i2 = tee(r)

print('i1:', list(i1))
print('i2:', list(i2))
```

`tee()` a une sémantique similaire à l'utilitaire `tee` Unix, qui répète la valeur qu'il lit depuis sont entrée et l'écrit dans un fichier nommé et dans la sortie standard. Les itérateurs renvoyés par `tee()` peuvent être utilisés pour alimenter le même ensemble de données dans plusieurs algorithmes à traiter en parallèle.

```shell
$ python3 itertools_tee.py

i1: [0, 1, 2, 3, 4]
i2: [0, 1, 2, 3, 4]
```

Les nouveaux itérateurs créés par `tee()` partagent leur entrée, ainsi l'itérateur d'origine ne devrait pas être utilisé après la création des nouveaux.

```python
# itertools_tee_error.py
from itertools import *

r = islice(count(), 5)
i1, i2 = tee(r)

print('r:', end=' ')
for i in r:
    print(i, end=' ')
    if i > 1:
        break
print()

print('i1:', list(i1))
print('i2:', list(i2))
```

Si les valeurs sont consommées depuis l'entrée originale, les nouveaux itérateurs ne produiront pas ces valeurs :

```shell
$ python3 itertools_tee_error.py

r: 0 1 2
i1: [3, 4]
i2: [3, 4]
```

### Conversion d'Entrées #

La fonction native `map()` renvoie un itérateur qui appele une fonction sur les valeurs des itérateurs en entrée, et retourne le résultat. Il s'arrête quand l'itérateur d'entrée est épuisé.

```python
# itertools_map.py

def times_two(x):
    return 2 * x


def multiply(x, y):
    return (x, y, x * y)


print('Doubles:')
for i in map(times_two, range(5)):
    print(i)

print('\nMultiples:')
r1 = range(5)
r2 = range(5, 10)
for i in map(multiply, r1, r2):
    print('{:d} * {:d} = {:d}'.format(*i))

print('\nStopping:')
r1 = range(5)
r2 = range(2)
for i in map(multiply, r1, r2):
    print(i)
```

Dans ce premier exemple, la fonction lambda multiplie les valeurs entrées par 2. Dans un second exemple, la fonction lambda multiplie deux arguments, pris depuis des itérateurs séparés, et renvoie un tuple avec les arguments originaux et la valeur calculée. Le troisième exemple s'arrête après la production de deux tuples puisque la seconde intervalle est épuisée.

```shell
$ python3 itertools_map.py

Doubles:
0
2
4
6
8

Multiples:
0 * 5 = 0
1 * 6 = 6
2 * 7 = 14
3 * 8 = 24
4 * 9 = 36

Stopping:
(0, 0, 0)
(1, 1, 1)
```

La fonction `starmap()` est similaire à `map()`, mais au lieu de construire un tuple depuis de multiples itérateurs, elle découpe les éléments dans un unique itérateur comme argument pour la fonction de correspondance, en utilisant la syntaxe `*`.

```python
# itertools_starmap.py
from itertools import *

values = [(0, 5), (1, 6), (2, 7), (3, 8), (4, 9)]

for i in starmap(lambda x, y: (x, y, x * y), values):
    print('{} * {} = {}'.format(*i))
```

Là où la fonction de correspondance à `map()` est appelée `f(i1, i2)`, la fonction de correspondance passé à `starmap()` est appelée `f(*i)`.

```shell
$ python3 itertools_starmap.py

0 * 5 = 0
1 * 6 = 6
2 * 7 = 14
3 * 8 = 24
4 * 9 = 36
```

### Produire de Nouvelles Valeurs #

La fonction `count()` renvoie un itérateur qui produit des entiers consécutifs, indéfiniment. Le premier nombre peut être passé en argument (zéro est celui par défaut). Il n'y a pas d'argument supérieur (voir la fonction native `range()` pour avoir plus de contrôle sur l'ensemble résultant).

```python
# itertools_count.py
from itertools import *

for i in zip(count(1), ['a', 'b', 'c']):
    print(i)
```

Cet exemple s'arrête parce que l'argument list est épuisé.

```shell
$ python3 itertools_count.py

(1, 'a')
(2, 'b')
(3, 'c')
```

Les arguments start et step à `count()` peuvent être n'importe quelles valeurs numériques qui peuvent être ajoutées ensemble.

```python
# itertools_count_step.py
import fractions
from itertools import *

start = fractions.Fraction(1, 3)
step = fractions.Fraction(1, 3)

for i in zip(count(start, step), ['a', 'b', 'c']):
    print('{}: {}'.format(*i))
```

Dans cet exemple, les points start et step sont des objets `Fraction` depuis le module `fraction`.

```shell
$ python3 itertools_count_step.py

1/3: a
2/3: b
1: c
```

La fonction `cycle()` renvoie un itérateur qui répéte le contenu des arguments qui lui sont données, indéfiniment. Puisqu'il se souvient de contenu complet de l'itérateur d'entrée, il peut consommer un peu de mémoire, si l'itérateur est grand.

```python
# itertools_cycle.py
from itertools import *

for i in zip(range(7), cycle(['a', 'b', 'c'])):
    print(i)
```
























*[API]: Application Programming Interface - Interface de Programmation Applicative
*[PEP]: Python Enhancement Proposals - Propositions d'Amélioration Python
